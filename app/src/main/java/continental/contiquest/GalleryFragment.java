package continental.contiquest;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Cristian on 31.10.2015.
 */
public class GalleryFragment extends android.support.v4.app.Fragment {
    View mView;

    ImageView qr1img;
    TextView qr1;
    ImageView qr2img;
    TextView qr2;
    ImageView qr3img;
    TextView qr3;
    ImageView qr4img;
    TextView qr4;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.fragment_gallery, container, false);

        qr1img = (ImageView) mView.findViewById(R.id.achievement1_img);
        qr1 = (TextView) mView.findViewById(R.id.QR1);

        qr2img = (ImageView) mView.findViewById(R.id.achievement2_img);
        qr2 = (TextView) mView.findViewById(R.id.QR2);

        qr3img = (ImageView) mView.findViewById(R.id.achievement3_img);
        qr3 = (TextView) mView.findViewById(R.id.QR3);

        qr4img = (ImageView) mView.findViewById(R.id.achievement4_img);
        qr4 = (TextView) mView.findViewById(R.id.QR4);

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());


        if(sharedPreferences.getBoolean("achievement_one", false)) {
            qr1.setText("QR1: FOUND");
            qr1img.setImageResource(R.drawable.treasure_25pts);
        }else {
            qr1.setText("QR1: NOT FOUND!!!!!!!!!");
        }

        if(sharedPreferences.getBoolean("achievement_two", false)) {
            qr2.setText("QR2: FOUND");
            qr2img.setImageResource(R.drawable.treasure_50pts);
        }else {
            qr2.setText("QR2: NOT FOUND!!!!!!!!!!!!");
        }

        if(sharedPreferences.getBoolean("achievement_three", false)){
            qr3.setText("QR3: FOUND");
            qr3img.setImageResource(R.drawable.treasure_100pts);
        }else {
            qr3.setText("QR3: NOT FOUND!!!!!!!!");
        }

        if(sharedPreferences.getBoolean("achievement_four", false)){
            qr4.setText("QR4: FOUND");
            qr4img.setImageResource(R.drawable.treasure_100pts);
        }else {
            qr4.setText("QR4: NOT FOUND!!!!!!!");
        }
        return mView;
    }

    private void showToast(String message) {
        Toast toast = Toast.makeText(getActivity(), message, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.TOP| Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }
}
