package continental.contiquest;

import android.content.SharedPreferences;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import java.util.Arrays;
import java.util.List;

/**
 * Created by Cristian on 31.10.2015.
 */
public class CameraFragment extends android.support.v4.app.Fragment {
    View mView;

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    TextView scanText;
    Button scanButton;

    ImageScanner scanner;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    List<String> allAchievements;

    static {
        System.loadLibrary("iconv");
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());
        editor = sharedPreferences.edit();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        mView = inflater.inflate(R.layout.fragment_camera, container, false);

        allAchievements = Arrays.asList(new AllAchievements().getAchievements());

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        /* Instance barcode scanner */
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(mView.getContext(), mCamera, previewCb, autoFocusCB);
        FrameLayout preview = (FrameLayout) mView.findViewById(R.id.cameraPreview);
        preview.addView(mPreview);

        scanText = (TextView) mView.findViewById(R.id.scanText);

//        scanButton = (Button) mView.findViewById(R.id.ScanButton);
//
//        scanButton.setOnClickListener(new View.OnClickListener() {
//            public void onClick(View v) {
//                if (barcodeScanned) {
//                    barcodeScanned = false;
//                    scanText.setText("Scanning...");
//                    mCamera.setPreviewCallback(previewCb);
//                    mCamera.startPreview();
//                    previewing = true;
//                    mCamera.autoFocus(autoFocusCB);
//                }
//            }
//        });
        return mView;
    }

    public void onPause() {
        super.onPause();
        releaseCamera();
    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };

    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {

                    for (String achievement : allAchievements) {
                        if (sym.getData().equals(achievement)) {
                            //SET ACHIEVEMENT
                            if (allAchievements.indexOf(achievement) == 0) {
                                //CODE: QR1
                                if (!(sharedPreferences.getBoolean("achievement_one", false))) {
                                    scanText.setText("ADDED " + sym.getData());
                                    editor.putBoolean("achievement_one", true);
                                    editor.commit();
                                    break;
                                } else {
                                    scanText.setText("WAS FOUND ALREADY " + sym.getData());
                                    editor.putBoolean("achievement_one", true);
                                    editor.commit();
                                    break;
                                }
                            }else if (allAchievements.indexOf(achievement) == 1) {
                                //CODE: QR2
                                if (!(sharedPreferences.getBoolean("achievement_two", false))) {
                                    scanText.setText("ADDED " + sym.getData());
                                    editor.putBoolean("achievement_two", true);
                                    editor.commit();
                                    break;
                                } else {
                                    scanText.setText("WAS FOUND ALREADY " + sym.getData());
                                    editor.putBoolean("achievement_two", true);
                                    editor.commit();
                                    break;
                                }
                            } else if (allAchievements.indexOf(achievement) == 2) {
                                //CODE: QR3
                                if (!(sharedPreferences.getBoolean("achievement_three", false))) {
                                    scanText.setText("ADDED " + sym.getData());
                                    editor.putBoolean("achievement_three", true);
                                    editor.commit();
                                    break;
                                } else {
                                    scanText.setText("WAS FOUND ALREADY " + sym.getData());
                                    editor.putBoolean("achievement_three", true);
                                    editor.commit();
                                    break;
                                }
                            } else if (allAchievements.indexOf(achievement) == 3) {
                                //CODE: QR4
                                if (!(sharedPreferences.getBoolean("achievement_four", false))) {
                                    scanText.setText("ADDED " + sym.getData());
                                    editor.putBoolean("achievement_four", true);
                                    editor.commit();
                                    break;
                                } else {
                                    scanText.setText("WAS FOUND ALREADY " + sym.getData());
                                    editor.putBoolean("achievement_four", true);
                                    editor.commit();
                                    break;
                                }
                            }
                        } else {
                            scanText.setText("INVALID CODE! ");
                        }

                    }
                    barcodeScanned = true;
                }
            }
        }
    };

    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };
}
